<?php
class User extends CI_Model{
	
	function __construct()
    {
         parent::__construct();
    }
	public function getAllUsers(){
		$this->load->database();
		$query = $this->db->query('SELECT * FROM user');
		return $query->result();
	}
	public function getUserByName($name){
		/*$query = $this->db->query('SELECT * FROM user WHERE name = "'.$name.'"');
		return $query->result();*/
		$data = array();
		$mysqli = new mysqli("localhost", "root", "", "test");
	    $stmt = $mysqli->prepare("SELECT name, email, about, id, pass FROM user WHERE name =?");
	    $stmt->bind_param("s", $name);
	    $stmt->execute();
	    $stmt->bind_result($data['name'], $data['email'], $data['about'], $data['id'], $data['pass']);
		$stmt->fetch();
	    $stmt->close();
	    $mysqli->close();
	    if($data['name'] == NULL){
	    	$data = '';
	    }
	    return $data;
	}
	public function getUserById($id){
		$query = $this->db->query('SELECT * FROM user WHERE id = '.$id);
		return $query->result();
	}
	public function insert($data){
		$email = '';
		$name = '';
		$about = '';
		$avatar = '';
		$pass = '';
		$result = '';
		foreach($data as $key=>$value){
			switch ($key){
    			case 'name':
    			    $name = $value;
    			    break;
    			case 'pass':
    				$pass = $value;
    			    break;
    			case 'about':
    				$about = $value;
    			    break;
    			case 'email':
    				$email = $value;
    			    break;
    			case 'avatar':
    				$avatar = $value;
    			    break;
    		}
		}
		if($name != ''){
			$query = $this->getUserByName($name);
			if($query != ''){
				$result['error'] = "User '".$name."' have already exists"; 
			}
			else{
				$data = array();
				$mysqli = new mysqli("localhost", "root", "", "test");
			    $stmt = $mysqli->prepare("INSERT INTO user(name,email,about,pass,avatar) 
					                      VALUES(?,?,?,?,?)");
			    $stmt->bind_param("sssss", $name, $email, $about, $pass, $avatar);
			    $stmt->execute();
			    $stmt->close();
				$result['content'] = 'Insert Completed!';
			}
		}
		else{
			$result['error'] = "You have to enter user name";
		}
		
		return $result;
	}
	public function update($data, $name){
		$arr = array();
		foreach($data as $key=>$value){
			switch ($key){
    			case 'name':
    			    array_push($arr, $key."='".$value."' ");
    			    break;
    			case 'pass':
    				array_push($arr, $key."='".$value."' ");
    			    break;
    			case 'about':
    				array_push($arr, $key."='".$value."' ");
    			    break;
    			case 'email':
    				array_push($arr, $key."='".$value."' ");
    			    break;
    			case 'avatar':
    				array_push($arr, $key."='".$value."' ");
    			    break;
    		}
		}
		$str = implode(", ",$arr);
		$query = $this->db->query("UPDATE user SET ".$str." WHERE name = '".$name."'");
		if($query){
			$result['content'] = "Update Completed!";
			$result['data'] = $arr;
		}
		return $result;
	}
	public function uploadAvatar($name, $user){
		$this->db->query("UPDATE user SET avatar ='".$name."' WHERE name = '".$user."'");
	}

}
