<?php
defined('BASEPATH') OR EXIT('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
header('Content-Type: application/json');
class UserController extends CI_Controller{
	function index(){
		$this->load->model('user');
		$query = $this->user->getUserById(1);
		echo json_encode($query);
	}
	public function user(){
	 	$data = NULL;
	 	$this->load->model('user');
	 	$user = $this->uri->segment(3);
	 	$avatar = $this->uri->segment(4);
	 	$query = $this->user->getUserByName($user);
	 	switch ($_SERVER['REQUEST_METHOD']) {
	 	 	case 'GET':
	 	 	    switch($user){
	 	 	    	case 'wsgroup':
	 	 	    	    $result = $this->auth();
	 	 	    	    if($result == 1){
	 	 	    	    	$data = $query;
	 	 	    	    }
	 	 	    	    else{
	 	 	    	    	$data['error'] = "Basic Auth isn't correct!";
	 	 	    	    }
	 	 	    	    break;
	 	 	    	case NULL:
	 	 	    	    $data['error'] = 'You must enter user name';
	 	 	    	    break;
	 	 	    	default:
	 	 	    	    $pass = NULL;
	 	 	    		if($query == NULL){
	 	 	    			$data['error'] = "User '".$user."' doesn't exist!";
	 	 	    		}
	 	 	    		else{
	 	 	    			if(isset($_GET['password'])){
	 	 	    				$result = $this->checkUser($query, $_GET['password']);
	 	 	    				if($result == 1){
	 	 	    					$data = $query;
	 	 	    				}
	 	 	    				else{
	 	 	    					$data['error'] = "Invalid username or password";
	 	 	    				}
	 	 	    			}
	 	 	    			else{
	 	 	    				$data['error'] = 'You must enter password';
	 	 	    			}
	 	 	    		}
	 	 	    	    break;
	 	 	    }
	 	 		break;
	 	 	case 'POST':
	 	 	    if($avatar == 'avatar'){
	 	 	    	if(isset($_POST['password'])){
 	    				$result = $this->checkUser($query, $_POST['password']);
 	    				if($result == 1){
 	    					header('Content-Type:multipart/form-data');
 	    					$data = $this->uploadAvatar($user);
 	    				}
 	    				else{
 	    					$data['error'] = "Invalid username or password";
 	    				}
 	    			}
 	    			else{
 	    				$data['error'] = 'You must enter password';
 	    			}
 	    		}
	 	 	    else{
	 	 	    	if($_POST!=NULL){
		 	 	    	$data = $this->getData($_POST);
		 	 	    	if($data!=NULL && !isset($data['error'])){
		 	 	    		$data = $this->user->insert($data);
		 	 	    	}
		 	 	    }
		 	 	    else{
		 	 	    	$data['error'] = 'You must provide user information : name, pass, email, about, avatar'; 
		 	 	    }
	 	 	    }
	 	 	    
	 	 	    break;
	 	 	case 'PUT':
				$arr = preg_split('/------WebKitFormBoundary.*\nContent-Disposition: form-data; name=/', file_get_contents("php://input"));
				$_PUT = array();
				foreach($arr as $input) {
					preg_match('/"([^"]+)"/', $input, $key);
					$input = preg_replace('/------WebKitFormBoundary.*--/', '', $input);
					if($key!=NULL){
						$_PUT[$key[1]] = trim(str_replace($key[0], '', $input));
					}
					
				}
				if(isset($_PUT['password'])){
    				$result = $this->checkUser($query, $_PUT['password']);
    				if($result == 1){
    					$data = $this->getData($_PUT);
						if($data!=NULL && !isset($data['error'])){
		 	 	    		$data = $this->user->update($data, $user);
		 	 	    	}
    				}
    				else{
    					$data['error'] = "Invalid username or password";
    				}
    			}
    			else{
    				$data['error'] = 'You must enter password';
    			}
				
	 	 	    break;
	 	 	default:

	 	 		break;
	 	 }
	 	echo json_encode($data);
    }
    function auth(){
    	if (isset($_SERVER['PHP_AUTH_USER']) && $_SERVER['PHP_AUTH_USER'] == 'wsgroup' &&

					isset($_SERVER['PHP_AUTH_PW']) && $_SERVER['PHP_AUTH_PW'] == 'proudtobehere') {
		 			
		 			header('WWW-Authenticate: Basic realm="test"');

					header('HTTP/1.0 401 Unauthorized');

					return 1;
		}
		return 0;
    }
    function checkUser($result, $pass){
    	if($pass != '' && $pass == $result['pass']){
    		return 1;
    	}
    	return 0;
    }
    function getData($data){
    	$result = array();
    	foreach($data as $key=>$value){
    		switch ($key){
    			case 'name':
    			    $result['name'] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
    			    break;
    			case 'pass':
    				$result['pass'] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
    			    break;
    			case 'about':
    				$result['about'] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
    			    break;
    			case 'email':
    				$result['email'] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
    			    break;
    			case 'avatar':
    				$result['avatar'] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
    			    break;
    			case 'password':
    			    break;
    			default:
    			    $result = NULL;
    			    $result['error'] = "Unknown column '".$key."' in field list";
    			    break;
    		}
    	}
    	return $result;
    }
    function uploadAvatar($user){
    	$data = '';
    	// echo realpath("images/");
    	// die();
    	if(isset($_FILES['avatar'])){
    		if($_FILES['avatar']['name'] != NULL){
    			$path = $_SERVER['DOCUMENT_ROOT']."/test/images/";
    			$tmp_name = $_FILES['avatar']['tmp_name'];
    			$name = $_FILES['avatar']['name'];
    			$type = $_FILES['avatar']['type']; 
                $size = $_FILES['avatar']['size'];
                $this->user->uploadAvatar($name, $user); 
                move_uploaded_file($tmp_name, $path.$name);
                $data['content'] = "Upload Avatar Successfully!";
    		}
    		else{
	    		$data['error'] = 'Please choose an image for avatar';
	    	}
    	}
    	else{
    		$data['error'] = 'Please choose an image for avatar';
    	}
    	return $data;
    }
}
